﻿using HaulageCalendar.Abstractions;
using HaulageCalendar.Factories;
using HaulageCalendar.Helpers;
using Prism;
using Prism.Ioc;
using System.Diagnostics;
using System.IO;

namespace HaulageCalendar.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();
            
            LoadApplication(new HaulageCalendar.App(new UwpInitializer()));
        }
    }

    public class UwpInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            string path = Windows.Storage.ApplicationData.Current.LocalFolder.Path;
            path = Path.Combine(path, "haulage_db.sqlite");
            containerRegistry.RegisterInstance<IUnitOfWorkFactory>(new UnitOfWorkFactory(path));
            containerRegistry.RegisterInstance<IDBContextInitializer>(new SQLiteDBContextInitializer(path));
        }
    }
}
