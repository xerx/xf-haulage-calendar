﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using HaulageCalendar.Abstractions;
using HaulageCalendar.Factories;
using HaulageCalendar.Helpers;
using Prism;
using Prism.Ioc;

namespace HaulageCalendar.Droid
{
    [Activity(Label = "Haulage Calendar", Icon = "@mipmap/ic_launcher", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            Window.AddFlags(WindowManagerFlags.Fullscreen);
            RequestedOrientation = ScreenOrientation.Portrait;

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App(new AndroidInitializer()));
        }
    }

    public class AndroidInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            path = System.IO.Path.Combine(path, "haulage_db.sqlite");
            containerRegistry.RegisterInstance<IUnitOfWorkFactory>(new UnitOfWorkFactory(path));
            containerRegistry.RegisterInstance<IDBContextInitializer>(new SQLiteDBContextInitializer(path));
        }
    }
}

