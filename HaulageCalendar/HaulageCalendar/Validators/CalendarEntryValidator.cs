﻿using HaulageCalendar.Abstractions;
using HaulageCalendar.Models;

namespace HaulageCalendar.Validators
{
    public class CalendarEntryValidator : GenericValidator<CalendarEntry>
    {
        public override IValidator<CalendarEntry> Item(CalendarEntry item)
        {
            base.Item(item);
            IsValid = Should(entry => entry.Summary.Trim().Length > 0)
                        .Should(entry => entry.Summary.Trim().Length < 256)
                            .Should(entry => entry.Location.Trim().Length > 0)
                                .Should(entry => entry.Location.Trim().Length < 256)
                                    .Should(entry => entry.DateRange.Start <= entry.DateRange.End)
                                        .ShouldNot(entry => entry.Color == null)
                                            .IsValid;
            return this;
        }
    }
}
