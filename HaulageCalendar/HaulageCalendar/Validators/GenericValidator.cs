﻿using HaulageCalendar.Abstractions;
using System;

namespace HaulageCalendar.Validators
{
    public class GenericValidator<T> : IValidator<T>
    {
        protected T item;
        public bool IsValid { get; protected set; }

        public virtual IValidator<T> Item(T item)
        {
            this.item = item;
            IsValid = true;
            return this;
        }

        public IValidator<T> Should(Func<T, bool> predicate)
        {
            IsValid = Invoke(predicate) && IsValid;
            return this;
        }
        public IValidator<T> ShouldNot(Func<T, bool> predicate)
        {
            IsValid = !Invoke(predicate) && IsValid;
            return this;
        }
        protected virtual bool Invoke(Func<T, bool> predicate)
        {
            try
            {
                return predicate.Invoke(item);
            }
            catch(Exception)
            {
                return false;
            }
        }
    }
}
