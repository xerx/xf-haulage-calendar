﻿using HaulageCalendar.Abstractions;
using HaulageCalendar.Engine.Abstractions;
using HaulageCalendar.Engine.Extensions;
using HaulageCalendar.Models;
using HaulageCalendar.Persistence;
using HaulageCalendar.Persistence.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HaulageCalendar.Managers
{
    public class MonthCalendarManager : ICalendarManager
    {
        private readonly ICalendarBuilder calendarBuilder;
        private readonly ICalendarDayFactory dayFactory;
        private readonly IUnitOfWorkFactory unitOfWorkFactory;
        private DateRange currentRange;
        private DateRange visibleRange;
        public List<CalendarDay> CurrentDays { get; private set; }
        public DateTime CurrentDate
        {
            get
            {
                if(currentRange != null)
                {
                    return currentRange.Start;
                }
                return DateTime.Today;
            }
        }

        public MonthCalendarManager(ICalendarBuilder calendarBuilder, ICalendarDayFactory dayFactory, IUnitOfWorkFactory unitOfWorkFactory)
        {
            this.calendarBuilder = calendarBuilder;
            this.dayFactory = dayFactory;
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public Task Today()
        {
            return SetRange(DateTime.Today);
        }
        public Task Update()
        {
            return SetRange(currentRange.Start);
        }
        private async Task CreateCalendarDays(DateTime date)
        {
            List<DateTime?> days = calendarBuilder.GetDays(date).ToList();
            UpdateVisibleRange(days);
            CurrentDays = new List<CalendarDay>();
            RepoResult<IEnumerable<CalendarEntry>> result = await GetCurrentEntries();
            if(result.Success)
            {
                for (int i = 0; i < days.Count; i++)
                {
                    CurrentDays.Add(dayFactory.CreateDay(days[i],
                                                         currentRange,
                                                         result.Data.Where(e => e.DateRange.Contains(days[i].Value))));
                }
            }
        }

        private void UpdateVisibleRange(List<DateTime?> days)
        {
            bool notNullPredicate(DateTime? date) => date != null;
            visibleRange = new DateRange
            {
                Start = days.First(notNullPredicate).Value,
                End = days.Last(notNullPredicate).Value
            };
        }

        private async Task<RepoResult<IEnumerable<CalendarEntry>>> GetCurrentEntries()
        {
            RepoResult<IEnumerable<CalendarEntry>> result;
            IUnitOfWork unitOfWork = unitOfWorkFactory.CreateUnitOfWork();
            using (unitOfWork)
            {
                result = await unitOfWork.CalendarEntries.GetEntriesWithDateRange(visibleRange);
            }
            return result;
        }
        private Task SetRange(DateTime date)
        {
            currentRange = new DateRange
            {
                Start = date.GetFirstDayOfMonth(),
                End = date.GetLastDayOfMonth()
            };
            return CreateCalendarDays(currentRange.Start);
        }
        public Task NextRange()
        {
            if (NextRangeExists)
            {
                return SetRange(currentRange.End.AddDays(1));
            }
            return Task.CompletedTask;
        }

        public Task PreviousRange()
        {
            if (PreviousRangeExists)
            {
                return SetRange(currentRange.Start.AddDays(-1));
            }
            return Task.CompletedTask;
        }


        public bool PreviousRangeExists => currentRange.Start.HasPreviousMonth();
        public bool NextRangeExists => currentRange.End.HasNextMonth();

    }
}
