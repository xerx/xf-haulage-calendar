﻿using HaulageCalendar.Models;
using Prism.Events;

namespace HaulageCalendar.Events
{
    public class AppointmentDetailsRequested : PubSubEvent<CalendarEntry>
    {
    }
}
