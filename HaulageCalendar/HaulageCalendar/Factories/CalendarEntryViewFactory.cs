﻿using HaulageCalendar.Abstractions;
using HaulageCalendar.Views;

namespace HaulageCalendar.Factories
{
    public class CalendarEntryViewFactory : AbstractPoolingFactory<CalendarEntryView>, ICalendarEntryViewFactory
    {
        protected override CalendarEntryView CreateItem()
        {
            return new CalendarEntryView();
        }
    }
}
