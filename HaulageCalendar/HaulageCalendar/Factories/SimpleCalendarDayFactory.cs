﻿using HaulageCalendar.Abstractions;
using HaulageCalendar.Engine.Extensions;
using HaulageCalendar.Models;
using HaulageCalendar.Models.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;

namespace HaulageCalendar.Factories
{
    public class SimpleCalendarDayFactory : ICalendarDayFactory
    {
        public CalendarDay CreateDay(DateTime? date, DateRange dateRange, IEnumerable<CalendarEntry> entries)
        {
            CalendarDay calendarDay = new CalendarDay { Date = date };
            if (!date.HasValue) { return calendarDay; }
            if (date.Value.IsToday())
            {
                calendarDay.Type = DayType.Today;
            }
            else if (dateRange.Contains(date.Value))
            {
                calendarDay.Type = DayType.Focused;
            }
            if(entries != null)
            {
                calendarDay.Entries = new ObservableCollection<CalendarEntry>(entries?.OrderBy(entry => entry.DateRange.Start));
            }
            return calendarDay;
        }
    }
}
