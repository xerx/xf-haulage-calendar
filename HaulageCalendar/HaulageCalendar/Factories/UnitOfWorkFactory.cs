﻿using HaulageCalendar.Abstractions;
using HaulageCalendar.Persistence;
using HaulageCalendar.Persistence.Abstractions;

namespace HaulageCalendar.Factories
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly string dbPath;

        public UnitOfWorkFactory(string dbPath)
        {
            this.dbPath = dbPath;
        }
        public IUnitOfWork CreateUnitOfWork()
        {
            SQLiteDBContext dbContext = new SQLiteDBContext(dbPath);
            return new UnitOfWork(dbContext);
        }
    }
}
