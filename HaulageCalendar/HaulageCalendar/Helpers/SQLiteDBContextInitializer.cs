﻿using System.Threading.Tasks;
using HaulageCalendar.Abstractions;
using HaulageCalendar.Models;
using HaulageCalendar.Persistence;

namespace HaulageCalendar.Helpers
{
    public class SQLiteDBContextInitializer : IDBContextInitializer
    {
        private readonly string dbPath;

        public SQLiteDBContextInitializer(string dbPath)
        {
            this.dbPath = dbPath;
        }
        public async Task Initialize()
        {
            SQLiteDBContext context = new SQLiteDBContext(dbPath);
            await context.CreateTableAsync<CalendarEntry>();
            context.Dispose();
        }
    }
}
