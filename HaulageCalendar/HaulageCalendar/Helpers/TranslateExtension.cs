﻿using Plugin.Multilingual;
using System;
using System.Globalization;
using System.Resources;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HaulageCalendar.Helpers
{
    [ContentProperty("Text")]
    public class TranslateExtension : IMarkupExtension
    {
        private const string ResourceId = "HaulageCalendar.Resources.AppResources";

        private static readonly Lazy<ResourceManager> Manager = new Lazy<ResourceManager>(() => new ResourceManager(ResourceId, typeof(TranslateExtension).Assembly));

        public static string Translate(string text)
        {
            return Manager.Value.GetString(text);
        }

        public string Text { get; set; }


        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Text == null) { return ""; }

            CultureInfo cultureInfo = CrossMultilingual.Current.CurrentCultureInfo;

            string translation = Manager.Value.GetString(Text, cultureInfo);

            if (translation == null)
            {
#if DEBUG
                throw new ArgumentException($"Key '{Text}' was not found in resources '{ResourceId}' for culture '{cultureInfo.Name}'");
#else
				translation = Text;
#endif
            }
            return translation;
        }
    }
}
