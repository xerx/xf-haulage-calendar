﻿using Xamarin.Forms;

namespace HaulageCalendar.Helpers
{
    public static class ViewHelper
    {
        #region Colors
        public static Color ColorBlue { get; } = Color.FromHex("#08415C");
        public static Color ColorGray { get; } = Color.FromHex("#6B818C");
        public static Color ColorRed { get; } = Color.FromHex("#CC2936");
        public static Color ColorGreen { get; } = Color.FromHex("#003E1F");
        public static Color ColorBrokenWhite { get; } = Color.FromHex("#F1F0EA");
        public static Color ColorLightGray { get; } = Color.FromHex("#B0C0BC");
        #endregion

        #region Styles
        public static Style LabelStyle { get; } = new Style(typeof(Label))
        {
            Setters =
            {
                new Setter { Property = Label.TextColorProperty, Value = ColorGray },
                new Setter { Property = Label.FontSizeProperty, Value = FontHelper.NormalSize }
            }
        };
        public static Style EntryStyle { get; } = new Style(typeof(Xamarin.Forms.Entry))
        {
            Setters =
            {
                new Setter { Property = Label.TextColorProperty, Value = ColorGray },
                new Setter { Property = Label.FontSizeProperty, Value = FontHelper.NormalSize }
            }
        };
        public static Style ButtonStyle { get; } = new Style(typeof(Button))
        {
            Setters =
            {
                new Setter { Property = Button.BackgroundColorProperty, Value = ColorBlue },
                new Setter { Property = Button.CornerRadiusProperty, Value = 0 },
                new Setter { Property = Button.TextColorProperty, Value = Color.White }
            }
        };
        #endregion

        public static void Initialize()
        {
            App.Current.Resources.Add(LabelStyle);
            App.Current.Resources.Add(EntryStyle);
            App.Current.Resources.Add(ButtonStyle);
        }

    }
}
