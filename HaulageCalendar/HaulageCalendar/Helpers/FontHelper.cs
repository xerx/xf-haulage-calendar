﻿using System;
using Xamarin.Forms;

namespace HaulageCalendar.Helpers
{
    public static class FontHelper
    {
        public static float NanoFactor { get; set; } = .4f;
        public static float TinyFactor { get; set; } = .6f;
        public static float SmallFactor { get; set; } = .8f;
        public static float NormalFactor { get; set; } = 1f;
        public static float LargeFactor { get; set; } = 1.2f;
        public static float HugeFactor { get; set; } = 1.4f;
        public static float TeraFactor { get; set; } = 1.6f;

        public static double NanoSize => Math.Round(BaseSize * NanoFactor);
        public static double TinySize => Math.Round(BaseSize * TinyFactor);
        public static double SmallSize => Math.Round(BaseSize * SmallFactor);
        public static double NormalSize => Math.Round(BaseSize * NormalFactor);
        public static double LargeSize => Math.Round(BaseSize * LargeFactor);
        public static double HugeSize => Math.Round(BaseSize * HugeFactor);
        public static double TeraSize => Math.Round(BaseSize * TeraFactor);

        public static double BaseSize = Device.GetNamedSize(NamedSize.Default, typeof(Label));
    }
}
