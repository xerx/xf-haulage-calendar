﻿using System;

namespace HaulageCalendar.Abstractions
{
    public interface IValidator<T>
    {
        IValidator<T> Item(T item);
        IValidator<T> Should(Func<T, bool> predicate);
        IValidator<T> ShouldNot(Func<T, bool> predicate);
        bool IsValid { get; }
    }
}
