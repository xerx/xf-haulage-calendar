﻿using System.Collections.Generic;

namespace HaulageCalendar.Abstractions
{
    public abstract class AbstractPoolingFactory<T> : IPoolingFactory<T>
    {
        public int Count => pool.Count;
        protected Queue<T> pool;
        public AbstractPoolingFactory()
        {
            pool = new Queue<T>();
        }
       

        public T GetItem()
        {
            if(pool.Count > 0)
            {
                return pool.Dequeue();
            }
            return CreateItem();
        }
        public void ReturnItem(T item)
        {
            pool.Enqueue(item);
        }
        protected abstract T CreateItem();
    }
}
