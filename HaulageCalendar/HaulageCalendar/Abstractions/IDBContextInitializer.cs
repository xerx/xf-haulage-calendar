﻿using System.Threading.Tasks;

namespace HaulageCalendar.Abstractions
{
    public interface IDBContextInitializer
    {
        Task Initialize();
    }
}
