﻿namespace HaulageCalendar.Abstractions
{
    public interface IPoolingFactory<T>
    {
        T GetItem();
        void ReturnItem(T item);
        int Count { get; }
    }
}
