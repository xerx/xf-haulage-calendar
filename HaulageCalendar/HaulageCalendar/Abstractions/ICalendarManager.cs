﻿using HaulageCalendar.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HaulageCalendar.Abstractions
{
    public interface ICalendarManager
    {
        Task NextRange();
        Task PreviousRange();
        Task Today();
        Task Update();
        bool PreviousRangeExists { get; }
        bool NextRangeExists { get; }
        DateTime CurrentDate { get; }
        List<CalendarDay> CurrentDays { get; }
    }
}
