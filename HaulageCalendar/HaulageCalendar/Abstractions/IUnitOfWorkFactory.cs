﻿using HaulageCalendar.Persistence.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace HaulageCalendar.Abstractions
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork CreateUnitOfWork();
    }
}
