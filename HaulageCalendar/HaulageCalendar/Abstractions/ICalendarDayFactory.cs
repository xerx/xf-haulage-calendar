﻿using HaulageCalendar.Models;
using System;
using System.Collections.Generic;

namespace HaulageCalendar.Abstractions
{
    public interface ICalendarDayFactory
    {
        CalendarDay CreateDay(DateTime? date, DateRange dateRange, IEnumerable<CalendarEntry> entries);
    }
}
