﻿using HaulageCalendar.Views;

namespace HaulageCalendar.Abstractions
{
    public interface ICalendarEntryViewFactory : IPoolingFactory<CalendarEntryView>
    {
    }
}
