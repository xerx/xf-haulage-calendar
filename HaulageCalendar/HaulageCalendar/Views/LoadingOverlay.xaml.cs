﻿
using System;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HaulageCalendar.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoadingOverlay : ContentView
	{
        public bool IsActive
        {
            get { return (bool)GetValue(IsActiveProperty); }
            set { SetValue(IsActiveProperty, value); }
        }

        public static readonly BindableProperty IsActiveProperty =
            BindableProperty.Create(nameof(IsActive),
                typeof(bool), typeof(LoadingOverlay), false,
                propertyChanged:(b, o, n) => (b as LoadingOverlay).IsActiveChanged());

        private void IsActiveChanged()
        {
            indicator.IsRunning = IsActive;
            IsVisible = IsActive;
        }

        public LoadingOverlay ()
		{
			InitializeComponent ();
		}
	}
}