﻿
using HaulageCalendar.Abstractions;
using HaulageCalendar.Models;
using System.Collections.Generic;
using Prism.Ioc;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static Xamarin.Forms.Grid;
using System.Diagnostics;
using HaulageCalendar.Helpers;

namespace HaulageCalendar.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CalendarDayView : ContentView
	{
        public CalendarDay Day
        {
            get { return (CalendarDay)GetValue(DayProperty); }
            set { SetValue(DayProperty, value); }
        }

        public static readonly BindableProperty DayProperty =
            BindableProperty.Create(nameof(Day),
                typeof(CalendarDay),
                typeof(CalendarDayView),
                propertyChanged: (b, o, n) => (b as CalendarDayView).DayChanged());

        private readonly ICalendarEntryViewFactory entryViewFactory;

        private List<CalendarEntryView> entryViews;
        private CalendarDayMoreButton moreButton;

        private IGridList<View> GridChildren => (Content as Grid).Children;

        public CalendarDayView()
        {
            InitializeComponent();
            BindingContext = this;
            entryViewFactory = App.Current.Container.Resolve<ICalendarEntryViewFactory>();
            entryViews = new List<CalendarEntryView>();
        }

        private void DayChanged()
        {
            ClearAddedViews();
            for (int i = 0; i < 2; i++)
            {
                if (Day.Entries?.Count > i)
                {
                    CalendarEntryView entryView = entryViewFactory.GetItem();
                    entryView.Entry = Day.Entries[i];
                    GridChildren.Add(entryView, 0, i + 1);
                    entryViews.Add(entryView);
                }
            }
            if(Day.Entries?.Count > 2)
            {
                moreButton = new CalendarDayMoreButton();
                moreButton.Text = $"{Day.Entries.Count - 2} {TranslateExtension.Translate("More")}";
                moreButton.Day = Day;
                GridChildren.Add(moreButton, 0, 3);
            }
        }
        private void ClearAddedViews()
        {
            if (moreButton != null)
            {
                GridChildren.Remove(moreButton);
                moreButton = null;
            }
            for (int i = 0; i < entryViews.Count; i++)
            {
                entryViewFactory.ReturnItem(entryViews[i]);
                GridChildren.RemoveAt(GridChildren.Count - 1);
            }
            entryViews.Clear();
        }
	}
}