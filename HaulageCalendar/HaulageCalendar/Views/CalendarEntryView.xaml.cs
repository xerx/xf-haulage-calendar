﻿
using HaulageCalendar.Events;
using HaulageCalendar.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Ioc;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HaulageCalendar.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CalendarEntryView : ContentView
	{
        public CalendarEntry Entry
        {
            get { return (CalendarEntry)GetValue(EntryProperty); }
            set { SetValue(EntryProperty, value); }
        }

        public static readonly BindableProperty EntryProperty =
            BindableProperty.Create(nameof(Entry),
                typeof(CalendarEntry),
                typeof(CalendarEntryView));

        public DelegateCommand TapCommand { get; set; }
        public CalendarEntryView()
		{
            TapCommand = new DelegateCommand(AppointmentDetailsRequest);
            BindingContext = this;
			InitializeComponent ();
        }
        private void AppointmentDetailsRequest()
        {
            IEventAggregator eventAggregator = App.Current.Container.Resolve<IEventAggregator>();
            eventAggregator.GetEvent<AppointmentDetailsRequested>().Publish(Entry);
        }
    }
}