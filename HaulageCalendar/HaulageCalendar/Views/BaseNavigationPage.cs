﻿using HaulageCalendar.Helpers;
using Xamarin.Forms;

namespace HaulageCalendar.Views
{
    public class BaseNavigationPage : NavigationPage
    {
        public BaseNavigationPage()
        {
            BarBackgroundColor = ViewHelper.ColorBlue;
            BarTextColor = Color.White;
        }
    }
}
