﻿
using HaulageCalendar.Models;
using Prism.Events;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Prism.Ioc;
using HaulageCalendar.Events;
using Prism.Commands;
using System.Windows.Input;

namespace HaulageCalendar.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CalendarDayMoreButton : ContentView
	{
        public static readonly BindableProperty TextProperty =
            BindableProperty.Create(nameof(Text),
                                    typeof(string),
                                    typeof(CalendarDayMoreButton),
                                    propertyChanged:(b, n, o) => (b as CalendarDayMoreButton).TextChanged());
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
        public ICommand TapCommand { get; set; }
        public CalendarDay Day { get; set; }

        public CalendarDayMoreButton ()
        {
            TapCommand = new Command(DayDetailsRequest);
            BindingContext = this;
            InitializeComponent ();
		}
        private void TextChanged()
        {
            label.Text = Text;
        }

        private void DayDetailsRequest()
        {
            IEventAggregator eventAggregator = App.Current.Container.Resolve<IEventAggregator>();
            eventAggregator.GetEvent<DayDetailsRequested>().Publish(Day);
        }
    }
}