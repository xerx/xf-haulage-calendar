﻿using HaulageCalendar.Models;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HaulageCalendar.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CalendarView : ContentView
	{
        public List<CalendarDay> Days
        {
            get { return (List<CalendarDay>)GetValue(DaysProperty); }
            set { SetValue(DaysProperty, value); }
        }
        
        public static readonly BindableProperty DaysProperty =
            BindableProperty.Create(nameof(Days), 
                typeof(List<CalendarDay>), 
                typeof(CalendarView),
                propertyChanged:(b, o, n) => (b as CalendarView).DaysChanged((List<CalendarDay>)n));

        private void DaysChanged(List<CalendarDay> newValue)
        {
            if(newValue == null) { return; }
            for (int i = 0; i < newValue.Count; i++)
            {
                dayViews[i].Day = newValue[i];
            }
        }
        
        private List<CalendarDayView> dayViews;

        public CalendarView()
		{
			InitializeComponent ();
            CollectDayViews();
		}
        private void CollectDayViews()
        {
            dayViews = new List<CalendarDayView>();
            Grid content = Content as Grid;
            View child;
            for (int i = 0; i < content.Children.Count; i++)
            {
                child = content.Children[i];
                if(child is CalendarDayView)
                {
                    dayViews.Add(child as CalendarDayView);
                }
            }
        }
	}
}