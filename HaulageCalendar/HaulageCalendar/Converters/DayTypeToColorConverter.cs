﻿using HaulageCalendar.Helpers;
using HaulageCalendar.Models.Enums;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace HaulageCalendar.Converters
{
    public class DayTypeToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool.TryParse((string)parameter, out bool convertToTextColor);
            DayType dayType = (DayType)value;
            if(dayType == DayType.Today)
            {
                return convertToTextColor ? Color.White : ViewHelper.ColorRed;
            }
            if(dayType == DayType.Focused)
            {
                return convertToTextColor ? ViewHelper.ColorGray : Color.White;
            }
            if (dayType == DayType.Normal)
            {
                return convertToTextColor ? ViewHelper.ColorLightGray : Color.White;
            }
            return convertToTextColor ? ViewHelper.ColorGray : Color.White;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DayType.Normal;
        }
    }
}
