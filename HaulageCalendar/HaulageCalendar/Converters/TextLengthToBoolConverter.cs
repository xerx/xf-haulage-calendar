﻿using System;
using System.Diagnostics;
using System.Globalization;
using Xamarin.Forms;

namespace HaulageCalendar.Converters
{
    public class TextLengthToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value == null) { return false; }
            string text = ((string)value).Trim();
            return text.Length > 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
