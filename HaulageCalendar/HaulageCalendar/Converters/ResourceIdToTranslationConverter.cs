﻿using HaulageCalendar.Helpers;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace HaulageCalendar.Converters
{
    public class ResourceIdToTranslationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return TranslateExtension.Translate((string)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return "";
        }
    }
}
