﻿using HaulageCalendar.Abstractions;
using HaulageCalendar.Engine;
using HaulageCalendar.Engine.Abstractions;
using HaulageCalendar.Factories;
using HaulageCalendar.Helpers;
using HaulageCalendar.Managers;
using HaulageCalendar.Models;
using HaulageCalendar.Validators;
using HaulageCalendar.ViewModels;
using HaulageCalendar.Views;
using Prism;
using Prism.Ioc;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace HaulageCalendar
{
    public partial class App
    {
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }
        protected override async void OnInitialized()
        {
            InitializeComponent();
            ViewHelper.Initialize();

            await NavigationService.NavigateAsync("InitializationPage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<ICalendarBuilder, MonthMatrixBuilder>();
            containerRegistry.Register<ICalendarDayFactory, SimpleCalendarDayFactory>();
            containerRegistry.Register<IValidator<CalendarEntry>, CalendarEntryValidator>();
            containerRegistry.RegisterSingleton<ICalendarManager, MonthCalendarManager>();
            containerRegistry.RegisterInstance<ICalendarEntryViewFactory>(new CalendarEntryViewFactory());

            containerRegistry.RegisterForNavigation<BaseNavigationPage>();
            containerRegistry.RegisterForNavigation<CalendarPage, CalendarPageViewModel>();
            containerRegistry.RegisterForNavigation<InitializationPage, InitializationPageViewModel>();
            containerRegistry.RegisterForNavigation<DayDetailsPage, DayDetailsPageViewModel>();
            containerRegistry.RegisterForNavigation<CalendarEntryPage, CalendarEntryPageViewModel>();
        }
    }
}
