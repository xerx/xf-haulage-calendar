﻿using HaulageCalendar.Abstractions;
using HaulageCalendar.Models;
using HaulageCalendar.Persistence;
using HaulageCalendar.Persistence.Abstractions;
using HaulageCalendar.Views;
using Prism.Navigation;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

namespace HaulageCalendar.ViewModels
{
    public class DayDetailsPageViewModel : ViewModelBase
    {
        private CalendarDay day;
        public CalendarDay Day
        {
            get { return day; }
            set { SetProperty(ref day, value); }
        }
        private CalendarEntry selectedEntry;
        public CalendarEntry SelectedEntry
        {
            get => selectedEntry;
            set
            {
                selectedEntry = value;
                NavigateToEntry();
            }
        }
        private bool dayHasNoEntries;
        public bool DayHasNoEntries
        {
            get { return dayHasNoEntries; }
            set { SetProperty(ref dayHasNoEntries, value); }
        }
        private readonly IUnitOfWorkFactory unitOfWorkFactory;

        public DayDetailsPageViewModel(INavigationService navigationService, 
                                       IUnitOfWorkFactory unitOfWorkFactory) : base(navigationService)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }
        private async void NavigateToEntry()
        {
            if(SelectedEntry != null)
            {
                IsIdle = false;
                INavigationParameters parameters = new NavigationParameters();
                parameters.Add("entry", SelectedEntry);
                SelectedEntry = null;
                await NavigationService.NavigateAsync(nameof(CalendarEntryPage), parameters);
                IsIdle = true;
            }
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if(parameters.ContainsKey("day"))
            {
                Day = parameters.GetValue<CalendarDay>("day");
                await UpdateDayEntries();
            }
            else if(Day != null)
            {
                await UpdateDayEntries();
            }
            DayHasNoEntries = Day.Entries.Count == 0;
        }

        private async Task UpdateDayEntries()
        {
            using (IUnitOfWork unitOfWork = unitOfWorkFactory.CreateUnitOfWork())
            {
                RepoResult<IEnumerable<CalendarEntry>> result =
                    await unitOfWork.CalendarEntries.GetEntriesWithDateRange(
                        new DateRange { Start = Day.Date.Value, End = Day.Date.Value });
                if (result.Success)
                {
                    Day.Entries = new ObservableCollection<CalendarEntry>(result.Data);
                }
            }
            RaisePropertyChanged(nameof(Day));
        }
    }
}
