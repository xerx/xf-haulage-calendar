﻿using HaulageCalendar.Abstractions;
using HaulageCalendar.Helpers;
using HaulageCalendar.Models;
using Prism.Navigation;
using System;
using System.Windows.Input;
using Xamarin.Forms;
using Prism.Ioc;
using Prism.Services;
using HaulageCalendar.Persistence;
using System.Threading.Tasks;
using System.Diagnostics;

namespace HaulageCalendar.ViewModels
{
    public class CalendarEntryPageViewModel : ViewModelBase
    {
        public CalendarEntry Entry { get; set; }
        public bool IsEditingExistentEntry { get; set; }
        public ICommand SaveCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand CancelCommand { get; set; }

        private DateTime startDate;
        public DateTime StartDate
        {
            get => startDate;
            set
            {
                if (value > Entry.DateRange.End)
                {
                    StartDate = startDate;
                    return;
                }
                Entry.DateRange.Start = startDate = value;
                RaisePropertyChanged();
            }
        }

        private DateTime endDate;
        public DateTime EndDate
        {
            get => endDate;
            set
            {
                if(value < Entry.DateRange.Start)
                {
                    EndDate = endDate;
                    return;
                }
                Entry.DateRange.End = endDate = value;
                RaisePropertyChanged();
            }
        }
        private string saveButtonText;
        public string SaveButtonText
        {
            get { return saveButtonText; }
            set { SetProperty(ref saveButtonText, value); }
        }
        private readonly IUnitOfWorkFactory unitOfWorkFactory;
        private readonly IPageDialogService dialogService;
        private readonly IValidator<CalendarEntry> validator;

        public CalendarEntryPageViewModel(INavigationService navigationService, 
                                          IUnitOfWorkFactory unitOfWorkFactory, 
                                          IPageDialogService dialogService, 
                                          IValidator<CalendarEntry> validator) : base(navigationService)
        {
            SaveCommand = new Command(SaveRequest, () => false);
            DeleteCommand = new Command(DeleteRequest);
            CancelCommand = new Command(CancelRequest);
            this.unitOfWorkFactory = unitOfWorkFactory;
            this.dialogService = dialogService;
            this.validator = validator;
        }

        private async void CancelRequest()
        {
            IsIdle = false;
            await NavigationService.GoBackAsync();
            IsIdle = true;
        }

        private async void DeleteRequest()
        {
            if (!await ConfirmAction("ConfirmDeletion"))
            {
                return;
            }
            IsIdle = false;
            string alertTitle;
            string alertMessage;
            bool success = false;
            using (var unitOfWork = unitOfWorkFactory.CreateUnitOfWork())
            {
                RepoResult<CalendarEntry> result = await unitOfWork.CalendarEntries.Remove(Entry);
                if (success = result.Success)
                {
                    alertTitle = TranslateExtension.Translate("Success");
                    alertMessage = TranslateExtension.Translate("EntryDeleted");
                }
                else
                {
                    alertTitle = TranslateExtension.Translate("TryAgain");
                    alertMessage = TranslateExtension.Translate("EntryDeleteError");
                }
            }
            await NotifyUserAndContinue(alertTitle, alertMessage, success);
        }

        private async void SaveRequest()
        {
            IsIdle = false;
            string alertTitle;
            string alertMessage;
            bool success = false;
            if (!validator.Item(Entry).IsValid)
            {
                alertTitle = TranslateExtension.Translate("TryAgain");
                alertMessage = TranslateExtension.Translate("FillFieldsPrompt");
            }
            else
            {
                using (var unitOfWork = unitOfWorkFactory.CreateUnitOfWork())
                {
                    RepoResult<CalendarEntry> result = await unitOfWork.CalendarEntries.Add(Entry);
                    if (success = result.Success)
                    {
                        alertTitle = TranslateExtension.Translate("Success");
                        alertMessage = IsEditingExistentEntry ? TranslateExtension.Translate("EntryUpdated") : TranslateExtension.Translate("EntrySaved");
                    }
                    else
                    {
                        alertTitle = TranslateExtension.Translate("TryAgain");
                        alertMessage = TranslateExtension.Translate("EntrySaveError");
                    }
                }
            }
            await NotifyUserAndContinue(alertTitle, alertMessage, success);
        }
        private async Task NotifyUserAndContinue(string title, string message, bool success)
        {
            await dialogService.DisplayAlertAsync(title, message, "OK");
            if (success)
            {
                IsIdle = false;
                await NavigationService.GoBackAsync();
                IsIdle = true;
            }
        }
        private async Task<bool> ConfirmAction(string message)
        {
            return await dialogService.DisplayAlertAsync(
                        TranslateExtension.Translate("Attention"),
                        TranslateExtension.Translate(message),
                        "OK",
                        TranslateExtension.Translate("Cancel"));
        }
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if (parameters.ContainsKey("entry"))
            {
                Entry = parameters.GetValue<CalendarEntry>("entry");
                Title = TranslateExtension.Translate("EditAppointment");
                SaveButtonText = TranslateExtension.Translate("Update");
                IsEditingExistentEntry = true;
            }
            else
            {
                Entry = new CalendarEntry();
                Title = TranslateExtension.Translate("NewAppointment");
                SaveButtonText = TranslateExtension.Translate("Save");
            }
            StartDate = Entry.DateRange.Start;
            EndDate = Entry.DateRange.End;
            RaisePropertyChanged(nameof(Entry));
            RaisePropertyChanged(nameof(IsEditingExistentEntry));
        }
    }
}
