﻿using HaulageCalendar.Abstractions;
using HaulageCalendar.Events;
using HaulageCalendar.Models;
using HaulageCalendar.Views;
using Prism.Commands;
using Prism.Events;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace HaulageCalendar.ViewModels
{
    public class CalendarPageViewModel : ViewModelBase
    {
        public DateTime CurrentDate => calendarManager.CurrentDate;
        public List<CalendarDay> CalendarDays => calendarManager.CurrentDays;

        public DelegateCommand PreviousButtonCommand { get; set; }
        public DelegateCommand NextButtonCommand { get; set; }
        public DelegateCommand TodayButtonCommand { get; set; }
        public DelegateCommand NewAppointmentButtonCommand { get; set; }

        private readonly ICalendarManager calendarManager;
        private readonly IEventAggregator eventAggregator;

        public CalendarPageViewModel(INavigationService navigationService, 
                                     ICalendarManager calendarManager,
                                     IEventAggregator eventAggregator) : base(navigationService)
        {
            this.calendarManager = calendarManager;
            this.eventAggregator = eventAggregator;
            eventAggregator.GetEvent<DayDetailsRequested>().Subscribe(ShowDayDetails);
            eventAggregator.GetEvent<AppointmentDetailsRequested>().Subscribe(ShowAppointmentDetails);
            PreviousButtonCommand = new DelegateCommand(PreviousCalendarRangeRequest, () => calendarManager.PreviousRangeExists);
            NextButtonCommand = new DelegateCommand(NextCalendarRangeRequest, () => calendarManager.NextRangeExists);
            TodayButtonCommand = new DelegateCommand(TodayCalendarRequest);
            NewAppointmentButtonCommand = new DelegateCommand(NewAppointmentRequest);
        }

        private async void PreviousCalendarRangeRequest()
        {
            IsIdle = false;
            await calendarManager.PreviousRange();
            RaiseChangeEvents();
            IsIdle = true;
        }
        private async void NextCalendarRangeRequest()
        {
            IsIdle = false;
            await calendarManager.NextRange();
            RaiseChangeEvents();
            IsIdle = true;
        }
        private async void TodayCalendarRequest()
        {
            IsIdle = false;
            await calendarManager.Today();
            RaiseChangeEvents();
            IsIdle = true;
        }

        private async void NewAppointmentRequest()
        {
            IsIdle = false;
            await NavigationService.NavigateAsync(nameof(CalendarEntryPage));
            IsIdle = true;
        }
        private async void ShowDayDetails(CalendarDay day)
        {
            IsIdle = false;
            INavigationParameters parameters = new NavigationParameters();
            parameters.Add("day", day);
            await NavigationService.NavigateAsync(nameof(DayDetailsPage), parameters);
            IsIdle = true;
        }
        private async void ShowAppointmentDetails(CalendarEntry entry)
        {
            IsIdle = false;
            INavigationParameters parameters = new NavigationParameters();
            parameters.Add("entry", entry);
            await NavigationService.NavigateAsync(nameof(CalendarEntryPage), parameters);
            IsIdle = true;
        }

        private void RaiseChangeEvents()
        {
            RaisePropertyChanged(nameof(CalendarDays));
            RaisePropertyChanged(nameof(CurrentDate));
            PreviousButtonCommand.RaiseCanExecuteChanged();
            NextButtonCommand.RaiseCanExecuteChanged();
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            IsIdle = false;
            await calendarManager.Update();
            RaiseChangeEvents();
            IsIdle = true;
        }
    }
}
