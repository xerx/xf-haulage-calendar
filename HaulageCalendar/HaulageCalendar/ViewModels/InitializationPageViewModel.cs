﻿using HaulageCalendar.Abstractions;
using HaulageCalendar.Models;
using Prism.Ioc;
using Prism.Navigation;
using System.Diagnostics;

namespace HaulageCalendar.ViewModels
{
    public class InitializationPageViewModel : ViewModelBase
	{
        public InitializationPageViewModel(INavigationService navigationService) : base(navigationService)
        {
        }
        public override async  void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            await App.Current.Container.Resolve<IDBContextInitializer>().Initialize();
            await App.Current.Container.Resolve<ICalendarManager>().Today();
            await NavigationService.NavigateAsync("/BaseNavigationPage/CalendarPage");
        }
    }
}
