﻿using System;
using System.Collections.Generic;

namespace HaulageCalendar.Engine.Abstractions
{
    public interface ICalendarBuilder
    {
        IEnumerable<DateTime?> GetDays(DateTime date);
    }
}
