﻿using HaulageCalendar.Engine.Abstractions;
using HaulageCalendar.Engine.Extensions;
using System;
using System.Collections.Generic;

namespace HaulageCalendar.Engine
{
    public class MonthMatrixBuilder : ICalendarBuilder
    {
        private static readonly int DaysInWeek = 7;
        private static readonly int WeeksInMonth = 6;

        private delegate DateTime? GetDayDelegate(ref DateTime date, int weekIndex, int dayIndex, int numPreviousMonthVisibleDays);

        public IEnumerable<DateTime?> GetDays(DateTime date)
        {
            List<DateTime?> calendar = new List<DateTime?>();

            int numPreviousMonthVisibleDays = GetBaseMondayDayIndex(date.GetMonthFirstDayOfWeek());
            DateTime countingDate = GetMatrixStartingDate(date, numPreviousMonthVisibleDays);

            GetDayDelegate getDayDelegate = CreateDayDelegate(date);

            for (int week = 0; week < WeeksInMonth; week++)
            {
                for (int day = 0; day < DaysInWeek; day++)
                {
                    calendar.Add(getDayDelegate(ref countingDate, week, day, numPreviousMonthVisibleDays));
                }
            }
            return calendar;
        }
        private DateTime GetMatrixStartingDate(DateTime date, int numPreviousMonthVisibleDays)
        {
            if (date.HasPreviousMonth())
            {
                date = date.AddMonths(-1);
                return new DateTime(date.Year,
                                    date.Month,
                                    date.GetNumDaysInMonth())
                                .AddDays(-(numPreviousMonthVisibleDays - 1));
            }
            return new DateTime(date.Year, date.Month, 1);
        }
        private GetDayDelegate CreateDayDelegate(DateTime date)
        {
            if (!date.HasPreviousMonth())
            {
                return GetMinMonthDay;
            }
            else if (!date.HasNextMonth())
            {
                return GetMaxMonthDay;
            }
            return GetMonthDay;
        }
        private DateTime? GetMinMonthDay(ref DateTime date, int weekIndex, int dayIndex, int numPreviousMonthVisibleDays)
        {
            if (weekIndex == 0 &&
                dayIndex < numPreviousMonthVisibleDays)
            {
                return null;
            }
            return GetMonthDay(ref date, weekIndex, dayIndex, numPreviousMonthVisibleDays);
        }
        private DateTime? GetMaxMonthDay(ref DateTime date, int weekIndex, int dayIndex, int numPreviousMonthVisibleDays)
        {
            if (date == DateTime.MinValue) { return null; }
            if (date.Month == DateTime.MaxValue.Month &&
                date.Day == DateTime.DaysInMonth(date.Year, date.Month))
            {
                DateTime? dayDate = date;
                date = DateTime.MinValue;
                return dayDate;
            }
            return GetMonthDay(ref date, weekIndex, dayIndex, numPreviousMonthVisibleDays);
        }
        private DateTime? GetMonthDay(ref DateTime date, int weekIndex, int dayIndex, int numPreviousMonthVisibleDays)
        {
            DateTime? dayDate = date;
            date = date.AddDays(1);
            return dayDate;
        }
        private int GetBaseMondayDayIndex(DayOfWeek day)
        {
            return day == DayOfWeek.Sunday ? 6 : (int)day - 1;
        }
    }
}
