﻿using System;

namespace HaulageCalendar.Engine.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool HasNextMonth(this DateTime date)
        {
            return date <= DateTime.MaxValue.AddMonths(-1);
        }
        public static bool HasPreviousMonth(this DateTime date)
        {
            return date >= DateTime.MinValue.AddMonths(1);
        }
        public static DayOfWeek GetMonthFirstDayOfWeek(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1).DayOfWeek;
        }
        public static DateTime GetFirstDayOfMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }
        public static DateTime GetLastDayOfMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, GetNumDaysInMonth(date));
        }
        public static int GetNumDaysInMonth(this DateTime date)
        {
            return DateTime.DaysInMonth(date.Year, date.Month);
        }
        public static bool IsToday(this DateTime date)
        {
            return DateTime.Now.Year == date.Year && DateTime.Now.DayOfYear == date.DayOfYear;
        }
    }
}
