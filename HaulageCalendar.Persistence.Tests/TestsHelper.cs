﻿using System;

namespace HaulageCalendar.Persistence.Tests
{
    public static class TestsHelper
    {
        public static string DbPath =>
            System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "haulage_test.sqlite");
    }
}
