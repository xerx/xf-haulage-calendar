﻿using HaulageCalendar.Persistence.Abstractions;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace HaulageCalendar.Persistence.Tests
{
    public class RepositoryTests : IDisposable
    {
        private readonly IRepository<TestEntity> repo;
        private readonly SQLiteDBContext context;

        public RepositoryTests()
        {
            context = new SQLiteDBContext(TestsHelper.DbPath);
            context.CreateTableAsync<TestEntity>().GetAwaiter().GetResult();
            repo = new Repository<TestEntity>(context);
        }

        [Fact]
        public void RepoResultShouldBeSuccessfulByDefault()
        {
            new RepoResult<TestEntity>().Success.ShouldBeTrue();
        }

        [Fact]
        public void Add_ShouldNotThrowErrorAndAlwaysReturnResult()
        {
            Should.NotThrow(repo.Add(null));
            repo.Add(null).Result.ShouldBeOfType(typeof(RepoResult<TestEntity>));
        }
        [Fact]
        public void Find_ShouldNotThrowErrorAndAlwaysReturnResult()
        {
            Should.NotThrow(repo.Find(null));
            repo.Find(null).Result.ShouldBeOfType(typeof(RepoResult<IEnumerable<TestEntity>>));
        }
        [Fact]
        public void Find_ShouldReturnFailedResultIfPredicateIsNull()
        {
            repo.Find(null).Result.Success.ShouldBeFalse();
        }
        [Fact]
        public void Get_ShouldNotThrowErrorAndAlwaysReturnResult()
        {
            Should.NotThrow(repo.Get(-1));
            repo.Get(-1).Result.ShouldBeOfType(typeof(RepoResult<TestEntity>));
        }
        [Fact]
        public void Get_ShouldReturnFailedResultIfEntityNonExistent()
        {
            repo.Get(-1).Result.Success.ShouldBeFalse();
        }
        [Fact]
        public void Get_ShouldReturnResultIfEntityExists()
        {
            repo.Add(new TestEntity()).GetAwaiter().GetResult();
            RepoResult<IEnumerable<TestEntity>> getAllResult = repo.GetAll().GetAwaiter().GetResult();
            int id = getAllResult.Data.First().Id;
            RepoResult<TestEntity> getResult =  repo.Get(id).GetAwaiter().GetResult();

            getResult.Data.ShouldNotBeNull();
            getResult.Data.Id.ShouldBe(id);
        }
        [Fact]
        public void GetAll_ShouldNotThrowErrorAndAlwaysReturnResult()
        {
            Should.NotThrow(repo.GetAll());
            repo.GetAll().Result.ShouldBeOfType(typeof(RepoResult<IEnumerable<TestEntity>>));
        }
        [Fact]
        public void Remove_ShouldNotThrowErrorAndAlwaysReturnResult()
        {
            Should.NotThrow(repo.Remove(null));
            repo.Remove(null).Result.ShouldBeOfType(typeof(RepoResult<TestEntity>));
        }
        [Fact]
        public void Remove_ShouldReturnFailedResultIfEntityNonExistent()
        {
            repo.Remove(new TestEntity()).Result.Success.ShouldBeFalse();
        }
        [Fact]
        public void Remove_ShouldReturnValidResultIfEntityExists()
        {
            repo.Add(new TestEntity()).GetAwaiter().GetResult();
            RepoResult<IEnumerable<TestEntity>> getAllResult = repo.GetAll().GetAwaiter().GetResult();
            int numEntitiesBefore = getAllResult.Data.Count();

            RepoResult<TestEntity> removeResult = repo.Remove(getAllResult.Data.First()).GetAwaiter().GetResult();
            getAllResult = repo.GetAll().GetAwaiter().GetResult();
            int numEntitiesAfter = getAllResult.Data.Count();

            removeResult.Success.ShouldBeTrue();
            numEntitiesAfter.ShouldBe(numEntitiesBefore - 1);
        }

        public void Dispose()
        {
            context.CloseAsync();
        }
    }
}
