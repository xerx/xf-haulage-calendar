﻿using HaulageCalendar.Models;
using SQLite;

namespace HaulageCalendar.Persistence.Tests
{
    public class TestEntity : IEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
    }
}
