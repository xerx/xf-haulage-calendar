﻿using Shouldly;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace HaulageCalendar.Persistence.Tests
{
    public class SQLiteDBContextTests
    {
        public SQLiteDBContextTests()
        {
            SQLiteDBContext context = new SQLiteDBContext(TestsHelper.DbPath);
            context.CreateTableAsync<TestEntity>().GetAwaiter().GetResult();
        }

        [Fact]
        public void Select_ShouldReturnEntityIfItExists()
        {
            SQLiteDBContext context = new SQLiteDBContext(TestsHelper.DbPath);
            TestEntity entity = new TestEntity();
            context.Insert(entity).GetAwaiter().GetResult();

            int entityId = context.SelectAll<TestEntity>().Result.First().Id;
            entity = context.Select<TestEntity>(entityId).GetAwaiter().GetResult();
            context.Dispose();
            entity.ShouldNotBeNull();
        }

        [Fact]
        public void Select_ShouldReturnNullIfEntityNonExistent()
        {
            SQLiteDBContext context = new SQLiteDBContext(TestsHelper.DbPath);
            TestEntity entity = context.Select<TestEntity>(-1).GetAwaiter().GetResult();
            context.Dispose();

            entity.ShouldBeNull();
        }

        [Fact]
        public void SelectAll_ShouldNotReturnNull()
        {
            SQLiteDBContext context = new SQLiteDBContext(TestsHelper.DbPath);
            IEnumerable<TestEntity> entities = context.SelectAll<TestEntity>().GetAwaiter().GetResult();
            context.Dispose();

            entities.ShouldNotBeNull();
        }

        [Fact]
        public void Where_ShouldNotReturnNull()
        {
            SQLiteDBContext context = new SQLiteDBContext(TestsHelper.DbPath);
            IEnumerable<TestEntity> entities = context.Where<TestEntity>(e => e.Id == -1).GetAwaiter().GetResult();
            context.Dispose();

            entities.ShouldNotBeNull();
        }

        [Fact]
        public void Insert_ShouldReturnOneIfEntityNonExistent()
        {
            SQLiteDBContext context = new SQLiteDBContext(TestsHelper.DbPath);
            TestEntity entry = new TestEntity();
            int rows = context.Insert(entry).GetAwaiter().GetResult();

            context.Dispose();

            rows.ShouldBe(1);
        }
        [Fact]
        public void Insert_ShouldUpdateIfEntityExists()
        {
            SQLiteDBContext context = new SQLiteDBContext(TestsHelper.DbPath);
            TestEntity entity = new TestEntity();
            context.Insert(entity).GetAwaiter().GetResult();
            IEnumerable<TestEntity> entities = context.SelectAll<TestEntity>().Result;
            int count = entities.Count();
            
            context.Insert(entities.First()).GetAwaiter().GetResult();
            entities = context.SelectAll<TestEntity>().Result;
            context.Dispose();

            entities.Count().ShouldBe(count);
        }


        [Fact]
        public void Delete_ShouldReturnZeroIfEntityNonExistent()
        {
            SQLiteDBContext context = new SQLiteDBContext(TestsHelper.DbPath);
            int rows = context.Delete(new TestEntity()).GetAwaiter().GetResult();
            context.Dispose();

            rows.ShouldBe(0);
        }
        [Fact]
        public void Delete_ShouldReturnOneIfEntityExists()
        {
            SQLiteDBContext context = new SQLiteDBContext(TestsHelper.DbPath);
            TestEntity entry = new TestEntity { Id = 1 };
            context.Insert(entry).GetAwaiter().GetResult();
            int rows = context.Delete(entry).GetAwaiter().GetResult();

            context.Dispose();

            rows.ShouldBe(1);
        }

    }
}
