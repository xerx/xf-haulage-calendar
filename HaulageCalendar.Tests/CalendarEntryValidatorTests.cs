﻿using HaulageCalendar.Models;
using HaulageCalendar.Validators;
using Shouldly;
using System;
using Xunit;

namespace HaulageCalendar.Tests
{
    public class CalendarEntryValidatorTests
    {
        private CalendarEntryValidator validator;

        public CalendarEntryValidatorTests()
        {
            validator = new CalendarEntryValidator();
        }
        [Theory]
        [InlineData(null, null, null, null, null)]
        [InlineData("summary", "location", "2019/03/07", "2019/02/07", "#FFFFFF")]
        [InlineData("summarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummarysummary",
                    "location", "2019/03/07", "2019/04/07", "#FFFFFF")]
        [InlineData("summary",
                    "locationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocationlocation",
                    "2019/03/07", "2019/04/07", "#FFFFFF")]
        [InlineData("   ", "   ", "2019/03/07", "2019/03/07", "#FFFFFF")]
        [InlineData("summary", "   ", "2019/03/07", "2019/03/07", "#FFFFFF")]
        [InlineData("", "location", "", "2019/03/07", "#FFFFFF")]
        [InlineData("", "", "", "2019/03/07", "#FFFFFF")]
        [InlineData("", "", "", "2019/03/07", "")]
        [InlineData("", "", null, "2019/03/07", null)]
        private void IsValid_ShouldReturnInValid(string summary, string location, string startDate, string endDate, string color)
        {
            DateTime.TryParse(startDate, out DateTime start);
            DateTime.TryParse(endDate, out DateTime end);
            var entry = new CalendarEntry
            {
                Summary = summary,
                Location = location,
                HexColor = color,
                DateRange = new DateRange
                {
                    Start = start,
                    End = end
                }
            };
            
            validator.Item(entry).IsValid.ShouldBe(false);
        }
        [Fact]
        private void IsValid_ShouldReturnValid()
        {
           var entry = new CalendarEntry
            {
                Summary = "summary",
                Location = "location",
                HexColor = "#FFFFFF",
                DateRange = new DateRange
                {
                    Start = DateTime.Now,
                    End = DateTime.Now.AddDays(1)
                }
            };

            validator.Item(entry).IsValid.ShouldBe(true);
        }
    }
}
