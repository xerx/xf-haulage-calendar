﻿using HaulageCalendar.Abstractions;
using Shouldly;
using Xunit;

namespace HaulageCalendar.Tests
{
    public class AbstractFactoryTests
    {
        private IPoolingFactory<TestObject> factory;

        public AbstractFactoryTests()
        {
            factory = new TestFactory();
        }
        [Fact]
        private void GetItem_ShouldAlwaysReturnItem()
        {
            for (int i = 0; i < 100; i++)
            {
                factory.GetItem().ShouldBeOfType<TestObject>();
            }
        }
        [Fact]
        private void ShouldPoolObjects()
        {
            var item = factory.GetItem();
            factory.ReturnItem(item);

            factory.GetItem().ShouldBe(item);
        }
    }
    public class TestFactory : AbstractPoolingFactory<TestObject>
    {
        protected override TestObject CreateItem()
        {
            return new TestObject();
        }
    }
}
