﻿using Xunit;
using Shouldly;
using HaulageCalendar.Factories;
using HaulageCalendar.Abstractions;
using System;
using HaulageCalendar.Models;
using HaulageCalendar.Models.Helpers;
using System.Collections.Generic;

namespace HaulageCalendar.Tests
{
    public class CalendarDayFactoryTests
    {
        private readonly ICalendarDayFactory factory;

        public CalendarDayFactoryTests()
        {
            factory = new SimpleCalendarDayFactory();
        }
        [Theory]
        [InlineData("2019/03/07", "2019/03/07", 0)]
        [InlineData("2019/03/07", "2019/03/09", 2)]
        private void Create_ShouldAlwaysReturnDay(string start, string end, int numEntries)
        {
            var range = new DateRange
            {
                Start = DateTime.Parse(start),
                End = DateTime.Parse(end)
            };
            var entries = new List<CalendarEntry>();
            for (int i = 0; i < numEntries; i++)
            {
                entries.Add(new CalendarEntry());
            }
            var date = DateTime.Today;
            factory.CreateDay(date, range, entries).ShouldBeOfType<CalendarDay>();
        }
        [Fact]
        private void Create_ShouldThrowNoError()
        {
            factory.CreateDay(null, null, null);
        }
    }
}
