﻿using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace HaulageCalendar.Engine.Tests
{
    public class MonthMatrixBuilderTests
    {
        private readonly MonthMatrixBuilder builder;
        public MonthMatrixBuilderTests()
        {
            builder = new MonthMatrixBuilder();
        }

        [Theory]
        [InlineData("0001/01/01", "0001/01/01", "0001/02/11")]
        [InlineData("9999/12/01", "9999/11/29", null)]
        [InlineData("2019/03/01", "2019/02/25", "2019/04/07")]
        [InlineData("2019/02/01", "2019/01/28", "2019/03/10")]
        [InlineData("2019/01/01", "2018/12/31", "2019/02/10")]
        private void GetDays_ShouldReturnCorrectBounds(string randomMonthDay, string firstCalendarDay, string lastCalendarDay)
        {
            DateTime date = DateTime.Parse(randomMonthDay);
            DateTime? firstDay = null;
            if (firstCalendarDay != null)
            {
                firstDay = DateTime.Parse(firstCalendarDay);
            }
            DateTime? lastDay = null;
            if (lastCalendarDay != null)
            {
                lastDay = DateTime.Parse(lastCalendarDay);
            }

            List<DateTime?> result = builder.GetDays(date).ToList();

            result.First().ShouldBe(firstDay);
            result.Last().ShouldBe(lastDay);
        }
        [Theory]
        [InlineData("0001/01/01", "0001/01/01")]
        [InlineData("2019/03/01", "2019/02/25")]
        private void GetDays_DaysShouldBeConsecutive(string randomMonthDay, string firstCalendarDay)
        {
            DateTime date = DateTime.Parse(randomMonthDay);

            IEnumerable<DateTime?> result = builder.GetDays(date);

            date = DateTime.Parse(firstCalendarDay);

            foreach(DateTime? day in result)
            {
                day.ShouldBe(date);
                date = date.AddDays(1);
            }
        }
    }
}
