﻿using HaulageCalendar.Models.Helpers;
using SQLite;

namespace HaulageCalendar.Models
{
    public class CalendarEntry : IEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        private string summary;
        public string Summary
        {
            get => summary?.Trim();
            set => summary = value;
        }
        private string location;
        public string Location
        {
            get => location?.Trim();
            set => location = value;
        }
        public string HexColor { get; set; }

        private string startDate;
        public string StartDate
        {
            set => startDate = value;
            get
            {
                if (dateRange != null)
                {
                    startDate = DateHelper.DateToISO8610String(dateRange.Start);
                }
                return startDate;
            }
        }
        private string endDate;
        public string EndDate
        {
            set => endDate = value;
            get
            {
                if (dateRange != null)
                {
                    endDate = DateHelper.DateToISO8610String(dateRange.End);
                }
                return endDate;
            }
        }

        private EntryColor color;
        [Ignore]
        public EntryColor Color
        {
            get
            {
                if (color == null)
                {
                    color = EntryColor.FromHex(HexColor);
                }
                return color;
            }
            set
            {
                color = value;
                HexColor = color.Value;
            }
        }
        private DateRange dateRange;

        [Ignore]
        public DateRange DateRange
        {
            get
            {
                if (dateRange == null)
                {
                    DateRange = new DateRange
                    {
                        Start = DateHelper.ISO8610StringToDate(StartDate),
                        End = DateHelper.ISO8610StringToDate(EndDate)
                    };
                }
                return dateRange;
            }
            set
            {
                value.Start = value.Start.Date;
                value.End = value.End.Date.AddDays(1).AddMinutes(-1);
                dateRange = value;
                StartDate = DateHelper.DateToISO8610String(dateRange.Start);
                EndDate = DateHelper.DateToISO8610String(dateRange.End);
            }
        }
    }
}
