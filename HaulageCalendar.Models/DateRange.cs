﻿using System;
using System.Diagnostics;

namespace HaulageCalendar.Models
{
    public class DateRange
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public bool Intersects(DateRange range)
        {
            return Contains(range.Start) || Contains(range.End) || IsContainedWithin(range);
        }
        public bool IsContainedWithin(DateRange range)
        {
            return Start >= range.Start && End <= range.End;
        }
        public bool Contains(DateTime value)
        {
            return Start <= value && value <= End;
        }
    }
}
