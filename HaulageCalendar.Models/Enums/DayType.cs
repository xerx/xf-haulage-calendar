﻿namespace HaulageCalendar.Models.Enums
{
    public enum DayType
    {
        Normal,
        Today,
        Focused,
        Holiday
    }
}
