﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HaulageCalendar.Models
{
    public class EntryColor
    {
        public static readonly EntryColor Blue = new EntryColor { Name = "Blue", Value = "#5E7C9D"  };
        public static readonly EntryColor LightBlue = new EntryColor { Name = "LightBlue", Value = "#5D98A4" };
        public static readonly EntryColor Green = new EntryColor { Name = "Green", Value = "#809D68" };
        public static readonly EntryColor Orange = new EntryColor { Name = "Orange", Value = "#D18953" };
        public static readonly EntryColor Black = new EntryColor { Name = "Black", Value = "#3A4B58" };
        public static readonly EntryColor Purple = new EntryColor { Name = "Purple", Value = "#804FA4" };
        public static readonly EntryColor Red = new EntryColor { Name = "Red", Value = "#FF6B6B" };

        public static List<EntryColor> AllColors
        {
            get
            {
                return typeof(EntryColor)
                        .GetFields(BindingFlags.Static | BindingFlags.Public)
                        .Select(info => info.GetValue(null) as EntryColor)
                        .ToList();
            }
        }
        public static EntryColor FromHex(string hex)
        {
            return AllColors.FirstOrDefault(color => color.Value == hex) ?? Blue;
        }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
