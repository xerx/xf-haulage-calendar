﻿using System;
using System.Globalization;

namespace HaulageCalendar.Models.Helpers
{
    public static class DateHelper
    {
        private static readonly string ISO8610Format = "yyyy-MM-ddTHH:mm:ssZ";
        public static string DateToISO8610String(DateTime date)
        {
            if (date == null) { return ""; }
            return date.ToUniversalTime().ToString(ISO8610Format);
        }
        public static DateTime ISO8610StringToDate(string iso8610String)
        {
            if (DateTime.TryParseExact(iso8610String,
                                      ISO8610Format,
                                      null,
                                      DateTimeStyles.AssumeUniversal,
                                      out DateTime date))
            {
                return date;
            }
            return DateTime.Now;
        }

    }
}
