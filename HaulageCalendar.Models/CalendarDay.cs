﻿using HaulageCalendar.Models.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HaulageCalendar.Models
{
    public class CalendarDay
    {
        public DateTime? Date { get; set; }
        public ObservableCollection<CalendarEntry> Entries { get; set; }
        public DayType Type { get; set; }
    }
}
