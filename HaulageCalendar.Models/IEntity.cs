﻿namespace HaulageCalendar.Models
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
