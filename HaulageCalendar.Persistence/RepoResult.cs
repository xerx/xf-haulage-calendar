﻿using System;

namespace HaulageCalendar.Persistence
{
    public class RepoResult<TEntity>
    {
        public TEntity Data { get; set; }
        public Exception Exception { get; set; }
        public bool Success { get; set; } = true;
    }
}
