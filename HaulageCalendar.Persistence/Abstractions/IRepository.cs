﻿using HaulageCalendar.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HaulageCalendar.Persistence.Abstractions
{
    public interface IRepository<TEntity> where TEntity : IEntity, new()
    {
        Task<RepoResult<TEntity>> Get(int id);
        Task<RepoResult<IEnumerable<TEntity>>> GetAll();
        Task<RepoResult<IEnumerable<TEntity>>> Find(Expression<Func<TEntity, bool>> predicate);
        Task<RepoResult<TEntity>> Add(TEntity entity);
        Task<RepoResult<TEntity>> Remove(TEntity entity);
    }
}
