﻿using System;
using System.Threading.Tasks;

namespace HaulageCalendar.Persistence.Abstractions
{
    public interface IUnitOfWork : IDisposable
    {
        ICalendarEntryRepository CalendarEntries { get; }
    }
}
