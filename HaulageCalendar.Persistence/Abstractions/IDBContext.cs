﻿using HaulageCalendar.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HaulageCalendar.Persistence.Abstractions
{
    public interface IDBContext
    {
        Task<int> Insert<TEntity>(TEntity entity) where TEntity : IEntity, new();
        Task<int> Delete<TEntity>(TEntity entity) where TEntity : IEntity, new();
        Task<TEntity> Select<TEntity>(int id) where TEntity : IEntity, new();
        Task<IEnumerable<TEntity>> SelectAll<TEntity>() where TEntity : new();
        Task<IEnumerable<TEntity>> Where<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : new();
        void Dispose();
    }
}
