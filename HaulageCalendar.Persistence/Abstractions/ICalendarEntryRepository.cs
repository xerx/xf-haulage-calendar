﻿using HaulageCalendar.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HaulageCalendar.Persistence.Abstractions
{
    public interface ICalendarEntryRepository : IRepository<CalendarEntry>
    {
        Task<RepoResult<IEnumerable<CalendarEntry>>> GetEntriesWithDateRange(DateRange date);
    }
}
