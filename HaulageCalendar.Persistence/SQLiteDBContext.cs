﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using HaulageCalendar.Models;
using HaulageCalendar.Persistence.Abstractions;
using SQLite;

namespace HaulageCalendar.Persistence
{
    public class SQLiteDBContext : SQLiteAsyncConnection, IDBContext
    {
        public SQLiteDBContext(string databasePath, bool storeDateTimeAsTicks = true, object key = null) : base(databasePath, storeDateTimeAsTicks, key)
        {
        }

        public SQLiteDBContext(string databasePath, SQLiteOpenFlags openFlags, bool storeDateTimeAsTicks = true, object key = null) : base(databasePath, openFlags, storeDateTimeAsTicks, key)
        {
        }

        public Task<int> Delete<TEntity>(TEntity entity) where TEntity : IEntity, new()
        {
            return DeleteAsync(entity);
        }
        
        public Task<int> Insert<TEntity>(TEntity entity) where TEntity : IEntity, new()
        {
            TEntity dbEntity = FindAsync<TEntity>(entity.Id).GetAwaiter().GetResult();
            if (dbEntity != null)
            {
                return UpdateAsync(entity);
            }
            return InsertAsync(entity);
        }

        public Task<TEntity> Select<TEntity>(int id) where TEntity : IEntity, new()
        {
            return FindAsync<TEntity>(id);
        }

        public async Task<IEnumerable<TEntity>> SelectAll<TEntity>() where TEntity : new()
        {
            return await Table<TEntity>().ToListAsync() as IEnumerable<TEntity>;
        }

        public async Task<IEnumerable<TEntity>> Where<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : new()
        {
            return await Table<TEntity>().Where(predicate).ToListAsync() as IEnumerable<TEntity>;
        }
        public void Dispose()
        {
            CloseAsync();
        }
    }
}
