﻿using HaulageCalendar.Models;
using HaulageCalendar.Persistence.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HaulageCalendar.Persistence
{
    public class CalendarEntryRepository : Repository<CalendarEntry>, ICalendarEntryRepository
    {
        public CalendarEntryRepository(IDBContext dbContext) : base(dbContext)
        {
        }

        public async Task<RepoResult<IEnumerable<CalendarEntry>>> GetEntriesWithDateRange(DateRange range)
        {
            RepoResult<IEnumerable<CalendarEntry>> result = 
                new RepoResult<IEnumerable<CalendarEntry>>
            {
                Data = new List<CalendarEntry>()
            };
            try
            {
                IEnumerable<CalendarEntry> entries = await dbContext.SelectAll<CalendarEntry>();
                List<CalendarEntry> data = new List<CalendarEntry>();
                foreach (CalendarEntry entry in entries)
                {
                    if (range.Intersects(entry.DateRange)) { data.Add(entry); }
                }
                result.Data = data;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.Success = false;
            }
            return result;
        }

    }
}
