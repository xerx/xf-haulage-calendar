﻿using HaulageCalendar.Models;
using HaulageCalendar.Persistence.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HaulageCalendar.Persistence
{
    public class MockCalendarEntryRepository : Repository<CalendarEntry>, ICalendarEntryRepository
    {
        public MockCalendarEntryRepository(IDBContext dbContext) : base(dbContext)
        {
        }
        
        public Task<RepoResult<IEnumerable<CalendarEntry>>> GetEntriesWithDateRange(DateRange date)
        {
            RepoResult<IEnumerable<CalendarEntry>> result = new RepoResult<IEnumerable<CalendarEntry>>
            {
                Data = new List<CalendarEntry>()
            };
            result.Data = new List<CalendarEntry>()
            {
                new CalendarEntry()
                {
                    Summary = "adfe ervg vggvg gvgfv g g gc fgcgfcgfc gfcgy cgfhc gf ytgfgf tftyfty fty yt gfc gfh chf ch cfghc fghc fhg cfhgcfhgcgfhhc fgc cfgh ", Location = "bwer g",
                    DateRange = new DateRange{Start = DateTime.Today,End=DateTime.Today},
                    HexColor = "#E2172C"
                },
                new CalendarEntry()
                {
                    Summary = "adfe er", Location = "bwer g",
                    DateRange = new DateRange{Start = DateTime.Today,End=DateTime.Today},
                    HexColor = "#E2172C"
                },
                new CalendarEntry()
                {
                    Summary = "adfe er", Location = "bwer g",
                    DateRange = new DateRange{Start = DateTime.Today,End=DateTime.Today},
                    HexColor = "#E2172C"
                },
                new CalendarEntry()
                {
                    Summary = "adfe er", Location = "bwer g",
                    DateRange = new DateRange{Start = DateTime.Today,End=DateTime.Today},
                    HexColor = "#fbcde3"
                },
                new CalendarEntry()
                {
                    Summary = "a1 wergwer re", Location = "bwerg ",
                    DateRange = new DateRange{Start = DateTime.Today.AddDays(-4),End=DateTime.Today},
                    HexColor = "#fbcde3"
                },
                new CalendarEntry()
                {
                    Summary = "a2werg reg ", Location = "bwer g",
                    DateRange = new DateRange{Start = DateTime.Today.AddDays(8),End=DateTime.Today.AddDays(9)},
                    HexColor = "#fbcde3"
                },
                new CalendarEntry()
                {
                    Summary = "a3wer g rg", Location = "bwer ",
                    DateRange = new DateRange{Start = DateTime.Today.AddDays(3),End=DateTime.Today.AddDays(3)},
                    HexColor = "#fbcde3"
                },
                new CalendarEntry()
                {
                    Summary = "a3", Location = "b",
                    DateRange = new DateRange{Start = DateTime.Today.AddMonths(1).AddDays(1),End=DateTime.Today.AddMonths(1).AddDays(1)},
                    HexColor = "#fbcde3"
                }
            };
            return Task.Run(() => result);
        }
        
    }
}
