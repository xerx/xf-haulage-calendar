﻿using HaulageCalendar.Models;
using HaulageCalendar.Persistence.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HaulageCalendar.Persistence
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : IEntity, new()
    {
        protected readonly IDBContext dbContext;

        public Repository(IDBContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<RepoResult<TEntity>> Add(TEntity entity)
        {
            try
            {
                await dbContext.Insert(entity);
                return new RepoResult<TEntity>();
            }
            catch (Exception ex)
            {
                return GetFailedResult<TEntity>(ex);
            }
        }

        public async Task<RepoResult<IEnumerable<TEntity>>> Find(Expression<Func<TEntity, bool>> predicate)
        {
            try
            {
                IEnumerable<TEntity> entities = await dbContext.Where(predicate);
                return new RepoResult<IEnumerable<TEntity>>
                {
                    Data = entities
                };
            }
            catch (Exception ex)
            {
                return GetFailedResult<IEnumerable<TEntity>>(ex);
            }
        }

        public async Task<RepoResult<TEntity>> Get(int id)
        {
            try
            {
                TEntity entity = await dbContext.Select<TEntity>(id);
                return new RepoResult<TEntity>
                {
                    Data = entity,
                    Success = entity != null
                };
            }
            catch (Exception ex)
            {
                return GetFailedResult<TEntity>(ex);
            }
        }

        public async Task<RepoResult<IEnumerable<TEntity>>> GetAll()
        {
            try
            {
                IEnumerable<TEntity> entities = await dbContext.SelectAll<TEntity>();
                return new RepoResult<IEnumerable<TEntity>>
                {
                    Data = entities
                };
            }
            catch (Exception ex)
            {
                return GetFailedResult<IEnumerable<TEntity>>(ex);
            }
        }

        public async Task<RepoResult<TEntity>> Remove(TEntity entity)
        {
            try
            {
                int rowsAffected = await dbContext.Delete(entity);
                return new RepoResult<TEntity>
                {
                    Success = rowsAffected > 0
                };
            }
            catch (Exception ex)
            {
                return GetFailedResult<TEntity>(ex);
            }
        }
        private RepoResult<T> GetFailedResult<T>(Exception ex)
        {
            return new RepoResult<T>
            {
                Exception = ex,
                Success = false
            };
        }
    }
}
