﻿using HaulageCalendar.Persistence.Abstractions;

namespace HaulageCalendar.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        public ICalendarEntryRepository CalendarEntries { get; private set; }
        private readonly IDBContext dbContext;

        public UnitOfWork(IDBContext dbContext)
        {
            this.dbContext = dbContext;
            CalendarEntries = new CalendarEntryRepository(this.dbContext);
        }
        public void Dispose()
        {
            dbContext.Dispose();
        }

    }
}
